# file docker-compose.yml
    - web-server: nginx có thể  sự dụng apache, https,...
    - php
    - networks
        - chia sẻ network cho cái container bằng tên code-network
# file Dockerfile
    - giúp tạo ra image để  tạo ra container
    
# chia nhỏ file docker-compose
    - docker-compose.nginx
    - docker-compose.php
    
# thay đổi port 
    - port 8080 thuộc file all có
    - port 8081 thuộc 2 file chia nhỏ
# lệnh run tất cả các docker-compose
    - docker-compose $(find docker* | sed -e 's/^/-f /') up
    -  Sed là một biên tập viên dòng.
        ^:Ký hiệu cho biết bắt đầu một dòng
        s:ký tự trắng
        -f: Filter output based on conditions provided
        $: Điểm kết thúc của dòng
        ():nhóm ký tự
        |: biểu thức thay thế

    docker-compose -f docker-compose.nginx.yml -f docker-compose.php.yml up -d
    -f : Specify an alternate compose file
<?php
include 'model.php';
include 'caching.php';

abstract class Creator
{
    abstract public function factoryMethod();
    public function run()
    {

        $network = $this->factoryMethod();
        $request = $_GET['value'];
        switch ($request) {
            case 'getData':
                $network->showItem();
                break;
            case 'postData':
                $network->addItem($_POST['strName']);
                break;
            case 'deleteData':
                $network->deleteItem($_POST['intId']);
                break;
            case 'updateData':
                $network->updateItem($_POST['intId'], $_POST['status']);
                break;
            default:
                echo "error request";
                break;
        }
    }
}

class ConcreteCreator1  extends Creator
{

    public function factoryMethod()
    {
        return new FacebookConnector();
    }
}

interface Imanager
{
    public function addItem($obj);

    public function showItem();

    public function deleteItem($obj);

    public function updateItem($obj);
}

class FacebookConnector implements Imanager
{
    function addItem($param)
    {
        $cache = new caching();
        $redis = $cache->Redis_connect();
        $vl = $redis->keys("List");
        if (!empty($vl)) {
            $vl = $redis->delete("List");
            echo "da xoa caching";
        }

        // $model = new model();
        $model = model::dataModel();
        $arrItem = $model->getData();
        $arr = $model->postData($param, $arrItem);
        try {

            if ($param != "") {
                $arrResponse = array(
                    "success" => true,
                    "code" => 200,
                    "data" => $arr,
                    "error" => NULL,
                    "message" => ""
                );
            } else {
                $arrResponse = array(
                    "success" => false,
                    "code" => 404,
                    "data" => null,
                    "error" => "error",
                    "message" => " Data not found"
                );
            }
            $json_response = json_encode($arrResponse);
            echo  $json_response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    function showItem()
    {
        $cache = new caching();
        $redis = $cache->Redis_connect();
        $output = null;

        $vl = $redis->keys("List");
        if (empty($vl)) {
            // $model = new model();
            $model = model::dataModel();
            $arrmodel = $model->getData();
            $output = json_encode($arrmodel);
            $redis->set("List", $output);
        } else {
            //neu khong tim thay key trong redis thi lay du lieu db
            $output = $redis->get("List");
        }
        try {
            if ($output != "") {
                $arrResponse = array(
                    "success" => true,
                    "code" => 200,
                    "data" => $output,
                    "error" => NULL,
                    "message" => ""
                );
            } else {
                $arrResponse = array(
                    "success" => false,
                    "code" => 404,
                    "data" => $output,
                    "error" => "error",
                    "message" => " Data not found"
                );
            }

            $json_response = json_encode($arrResponse);
            echo  $json_response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    //xoa phan tu truyen vao
    function deleteItem($param)
    {
        $cache = new caching();
        $redis = $cache->Redis_connect();
        $vl = $redis->keys("List");

        if (isset($vl)) {
            $vl = $redis->delete("List");
            echo "da xoa caching";
        }

        // $model = new model();
        $model = model::dataModel();

        $arrItem = $model->getData();
        $arr = $model->deleteData($param, $arrItem);

        try {
            if (!empty($param)) {
                if ($arr != "") {
                    $arrResponse = array(
                        "success" => true,
                        "code" => 200,
                        "data" => $arr,
                        "error" => NULL,
                        "message" => ""
                    );
                }
                $arrResponse = array(
                    "success" => false,
                    "code" => 404,
                    "data" => null,
                    "error" => "error",
                    "message" => " Data not found"
                );
            }
            echo  $arrResponse;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    //update todo
    function updateItem($arr)
    {

        try {
            // $model = new model();
            $model = model::dataModel();

            $arr = $model->updateData($arr['intId'], $arr['status']);
            if ($arr != "") {
                $arrResponse = array(
                    "success" => true,
                    "code" => 200,
                    "data" => $arr,
                    "error" => NULL,
                    "message" => ""
                );
            } else {
                $arrResponse = array(
                    "success" => false,
                    "code" => 404,
                    "data" => null,
                    "error" => "error",
                    "message" => " Data not found"
                );
            }

            $json_response = json_encode($arrResponse);
            echo  $json_response;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}


function clientCode(Creator $creator)
{
    $creator->run();
}

clientCode(new ConcreteCreator1());

<?php
class caching
{
    protected $connected;
    protected $redis;

    public function __construct()
    {
        $this->Redis_connect();
    }

    function Redis_connect()
    {
        try {
            $redis = new Redis();
            $connected = $redis->connect("redis", 6379);

            if (!$connected) {
                throw new Exception();
            }
            return  $redis;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}

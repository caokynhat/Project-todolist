<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/a.css">
    <title>Document</title>
</head>

<body>
    <div>
        <div class="mobile">
            <div class="tab">
                <div class="container">
                    <div class="trangchu">Trang Chu</div>
                    <div class="danhmuc">Danh mục</div>
                    <div class="tintuc">Tin tức</div>
                    <div class="gioithieu">Giới thiệu</div>
                </div>
            </div>

            <div class="InputTodo">
                <form id="fromItem" class="InputTodo" action="index.php" method="post">
                    <input type="text" id="myinput" name="name">
                    <button type="button" onclick="addItem()" class="addbtn">Click Me!</button>
                </form>
            </div><!--InputTodo-->

            <div class="ListTodo">
                <div id="listItem">
                </div><!--listItem-->

                <div id="listItem">
                    <div id="listTodoItem">
                    </div><!--listTodoItem-->
                </div><!--listItem-->
            </div><!--ListTodo-->
            
        </div>
    </div>
</body>

<script>
    //Click on a close button to hide the current list item
    var close = document.getElementsByClassName("close");
    for (var i = 0; i < close.length; i++) {
        close[i].onclick = function() {
            var a = document.querySelector("[data-id]");
            console.log(a);
            // div.style.display = "none";
            // deleteData(this.parentNode.id);
        }
    }

    // Create a new list item when clicking on the "Add" button
    function addItem() {
        //khoi tao node moi
        var newObj = document.createElement("h1");
        newObj.className = "loop";
        newObj.dataset.id = '1';
        var objspan = document.createElement("span");
        objspan.className = "item";
        //tim node input co ten la myinput
        var inputValue = document.getElementById("myinput").value;
        //tao noi dung cho node
        var inpputText = document.createTextNode(inputValue);
        //gan noi dung vao node
        objspan.appendChild(inpputText);
        newObj.appendChild(objspan);
        var a = document.querySelector("[data-id]");
        var value = document.getElementById("myinput").value;
        postData(value);

        //kiem tra neu gia tri input 
        if (inputValue === '') {
            alert("mời bạn nhập input ");
        } else {
            document.getElementById("listItem").appendChild(newObj);
        }

        document.getElementById("myinput").value = "";
        var span = document.createElement("SPAN");
        var txt = document.createTextNode("delete");
        span.className = "close";
        span.appendChild(txt);
        newObj.appendChild(span);

        for (i = 0; i < close.length; i++) {
            close[i].onclick = function() {
                var div = this.parentElement;
                div.style.display = "none";
                deleteData(this.parentNode.id);
            }
        }

        var span1 = document.createElement("SPAN");
        var txt1 = document.createTextNode("done");
        span1.className = "status";
        span1.appendChild(txt1);
        newObj.appendChild(span1);
        var element = document.getElementsByClassName("status");

        for (var i = 0; i < element.length; i++) {
            element[i].onclick = function() {
                if (a === 0) {
                    console.log("number", this.parentElement.id);
                    this.style.color = "red";
                    this.textContent = "undone";
                    updateData(this.parentNode.id, this.textContent);
                    a++;
                } else {
                    this.style.color = "green";
                    this.textContent = "done";
                    updateData(this.parentNode.id, this.textContent);
                    a--;
                }
            }
        }
        // end for 
        // location.reload();
    }

    //ajax post
    function postData(value) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                //var myArr = JSON.parse(this.responseText);
                //console.log(this.responseText);
            }
        };
        xhttp.open("POST", "http://localhost:8084/api.php?value=postData", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        value = document.getElementById("myinput").value;
        xhttp.send("strName=" + value);
    }

    // //get ajax  
    (function() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var myArr = JSON.parse(this.responseText);
                ShowItem(myArr['data']);
                //console.log(myArr['data']);
            }
        };

        xhttp.open("GET", "http://localhost:8084/api.php?value=getData");
        //cau hinh header cho request
        xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        //gui request
        xhttp.send();
        if (typeof(Storage) !== "undefined") {
            document.getElementById("myinput").innerHTML = localStorage.getItem("item");
        } else {
            document.getElementById("myinput").innerHTML = "Sorry, your browser does not support Web Storage...";
        }
    })();

    //delete ajax
    function deleteData(value) {
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "http://localhost:8084/api.php?value=deleteData", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("intId=" + value);
    }
    //hien thi item
    function ShowItem(data) {
        var arr = JSON.parse(data);
        console.log(arr)
        var text = "";
        for (var i = 0; i < arr.length; i++) {
            listItem += '<h3 class="listTableItem">' +
                '<span class="item">' + arr[i]['strName'] + '</span>' + '</h3>';

            text += '<h1 class="loop" id=' + arr[i]['intId'] + '>' +
                '<span class="item">' + arr[i]["strName"] + '</span>' +
                '<span class="close">' + "delete" + '</span>' +
                '<span class="status">' + "done" + '</span>' +
                '</h1>';
        }

        document.getElementById("listTodoItem").innerHTML = listItem;
        document.getElementById("listItem").innerHTML = text;

        var close = document.getElementsByClassName("close");
        var element = document.getElementsByClassName("status");
        var a = 0;
        for (var i = 0; i < element.length; i++) {
            element[i].onclick = function() {
                if (a === 0) {
                    this.style.color = "red";
                    this.textContent = "undone";
                    updateData(this.parentNode.id, this.textContent);
                    a++;
                } else {
                    this.style.color = "green";
                    this.textContent = "done";
                    updateData(this.parentNode.id, this.textContent);
                    a--;
                }
            }
        }

        for (i = 0; i < close.length; i++) {
            close[i].onclick = function() {
                var div = this.parentElement;
                div.style.display = "none";
                deleteData(this.parentNode.id);
            }
        }
    }

    //update ajax
    function updateData(id, value) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                //var myArr = JSON.parse(this.responseText);
                console.log(this.responseText);
            }
        };
        xhttp.open("POST", "http://localhost:8084/api.php?value=updateData", true);
        var data = [];
        data.push('intId=' + id);
        data.push('status=' + value);

        console.log("data", data);

        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(data);
    }
</script>

</html>